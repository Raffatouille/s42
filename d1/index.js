// Notes:
// The document refers to the whole webpage
// Toaccess specific object models from document we can use:
// document.querySelector('#txt-first-name')

// document.getElementById('#txt-first-name')
// document.getElemetByClassName('txt-inputs')
// document.getElementByTagName('input')


// document.firstName.innerHTML = firstName;
// console.log(Fname);
// document.txtLastName.innerHTML = lastName;
// console.log(Lname);

const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanLastName = document.querySelector('#span-last-name');

// FIRST NAME
// let firstName = txtFirstName.addEventListener('keyup', (event)=> {
//     spanFullName.innerHTML = txtFirstName.value ;
// })


// let Fname = txtFirstName.addEventListener('keyup', (event)=> {
//     console.log(event.target);
//     console.log(event.target.value); 
// } )

// LAST NAME
// let lastName = txtLastName.addEventListener('keyup', (event)=> {
//     spanFullName.innerHTML = txtLastName.value;
// })

// let Lname = txtLastName.addEventListener('keyup', (event)=> {
//     console.log(event.target);
//     console.log(event.target.value); 
// } )

const fullName = () => {
    let fName =txtFirstName.value;
    let lName = txtLastName.value;

    spanFullName.innerHTML = `${fName} ${lName}`;
}

txtFirstName.addEventListener('keyup', fullName);
txtLastName.addEventListener('keyup', fullName);
